using System;

public class PreciseHP: Health{

    //Use this for breakable stuff and things that are affected by percentage like
    public event Action<float, float> OnValueChanged;
    //Use this for characters and elements with clear disticnt hp. (Base fish have by example 3 HP)
    private float _value;
	private float maxHp;

	public float Value{
        get => _value;
        set {
            float prevValue = _value;
            _value = value;
            OnValueChanged?.Invoke(prevValue, _value);

            if(_value > prevValue){
                OnHeals.Invoke();
            }
            if(_value < Value){
                OnDamaged.Invoke();
            }

            if(_value <= 0){
                onHealthDepleted.Invoke();
            }
        }
    }

    public void Initialize(float newMaxHp = 100){
        maxHp = newMaxHp;
        Initialize();
    }

    public void Initialize(){
        _value = maxHp;
    }
}
