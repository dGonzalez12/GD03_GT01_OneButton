﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "FloatObject" ,  fileName = "float_")]
public class FloatObject : ScriptableObject{

    public event Action<float, float> OnValueChangedListener;

    [SerializeField]
    private float _value;
    public float Value{
        get => _value;

        set{
            var prevValue = _value;
            _value = value;
            OnValueChangedListener?.Invoke(prevValue, _value);
        }
    }
}
