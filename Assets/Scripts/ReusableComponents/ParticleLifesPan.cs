﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleLifesPan : MonoBehaviour
{
    ParticleSystem _par;

    private void Awake(){
        _par = GetComponent<ParticleSystem>();
        Destroy(gameObject,_par.main.duration);
    }

}
