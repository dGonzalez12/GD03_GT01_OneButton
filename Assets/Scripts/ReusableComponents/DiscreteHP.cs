using System;
using UnityEngine;

public class DiscreteHP : Health{

    public event Action<int, int> OnValueChanged;
    //Use this for characters and elements with clear disticnt hp. (Base fish have by example 3 HP)
    private int _value;
    public int Value{
        get => _value;
        set {
            int prevValue = _value;
            _value = value;
            _value = Mathf.Clamp(_value, 0, maxHp);
            OnValueChanged?.Invoke(prevValue, _value);
            if(_value > prevValue){
                OnHeals?.Invoke();
            }
            if(_value < prevValue){
                OnDamaged?.Invoke();
            }

            if(_value <= 0){
                onHealthDepleted?.Invoke();
            }
        }
    }
    [SerializeField]
    private int maxHp;

    public void Initialize(){
        _value = maxHp;
    }

    public void Initialize(int newMaxHp = 3){
        maxHp = newMaxHp;
        Initialize();
    }
}
