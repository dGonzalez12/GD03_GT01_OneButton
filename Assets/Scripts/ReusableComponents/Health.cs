using System;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour{

    public UnityEvent onHealthDepleted;

    public UnityEvent OnHeals;
    public UnityEvent OnDamaged;

    public void OnDamage(){
        OnDamaged.Invoke();
    }

    public void OnHealed(){
        OnHeals.Invoke();
    }

}
