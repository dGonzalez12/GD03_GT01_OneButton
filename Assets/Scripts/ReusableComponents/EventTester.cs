using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventTester: MonoBehaviour{

	[SerializeField]
	private List<EventKeyBind> events = new List<EventKeyBind>();

	private void Update(){
		foreach(EventKeyBind ev in events){
			if(!ev.holdKey){
				if(Input.GetKey(ev.keyCode)){
					ev.Event.Invoke();
				}
			}else{
				if(Input.GetKeyDown(ev.keyCode)){
					ev.Event.Invoke();
				}
			}
		}
		
	}

}
[Serializable]
public class EventKeyBind{

	public UnityEvent Event;
	public KeyCode keyCode;
	public bool holdKey = false;
}
