﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioMixerSettings : MonoBehaviour{

    [SerializeField]
    AudioMixer _mixer;

    [SerializeField]
    private float _threshold = 40f;

    [SerializeField] Slider _musicSlider;
    [SerializeField] Slider _uiSlider;
    [SerializeField] Slider _fxSlider;

    private void Start()
    {
        if(!PlayerPrefs.HasKey("MusicVol")){PlayerPrefs.SetFloat("MusicVol", 1f);}
        if(!PlayerPrefs.HasKey("UIVol")){PlayerPrefs.SetFloat("UIVol", 1f);}
        if(!PlayerPrefs.HasKey("FXVol")){PlayerPrefs.SetFloat("FXVol", 1f);}
        PlayerPrefs.Save();

        float mVolume = PlayerPrefs.GetFloat("MusicVol");
        _musicSlider.SetValueWithoutNotify(mVolume);
        SetMusicVolume(mVolume);

        float uiVol = PlayerPrefs.GetFloat("UIVol");
        _uiSlider.SetValueWithoutNotify(uiVol);
        SetUIVolume(uiVol);

        float fxVol = PlayerPrefs.GetFloat("FXVol");
        _fxSlider.SetValueWithoutNotify(fxVol);
        SetFXVolume(fxVol);

            
    }
    
    public void SetMusicVolume(float volume){
        _mixer.SetFloat("Music", Mathf.Log10(volume + 0.01f) * _threshold);
        PlayerPrefs.SetFloat("MusicVol", volume);
        PlayerPrefs.Save();

    }

    public void SetUIVolume(float volume){
        _mixer.SetFloat("UI", Mathf.Log10(volume + 0.01f) * _threshold);
        PlayerPrefs.SetFloat("UIVol", volume);
        PlayerPrefs.Save();


    }

    public void SetFXVolume(float volume){
        _mixer.SetFloat("FX", Mathf.Log10(volume + 0.01f) * _threshold);
        PlayerPrefs.SetFloat("FXVol", volume);
        PlayerPrefs.Save();

    }

}
