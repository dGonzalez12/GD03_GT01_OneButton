using UnityEngine;
using UnityEngine.EventSystems;

public class HighligthAnimationManager: MonoBehaviour, IPointerEnterHandler, IPointerExitHandler{

	[SerializeField]
	private HighligthAnimation[] animations;

	private void Awake(){
		animations = GetComponentsInChildren<HighligthAnimation>();
	}

	public void OnPointerEnter(PointerEventData eventData){
		foreach(HighligthAnimation animation in animations){
			animation.HighlighOn();
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		DisableAnimation();
	}

	private void OnDisable()
	{
		DisableAnimation();
	}

	private void DisableAnimation()
	{
		foreach (HighligthAnimation animation in animations)
		{
			animation.HighlightOff();
		}
	}
}

