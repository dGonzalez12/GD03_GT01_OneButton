using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class GameplayMenu : Menu {

    [SerializeField]
    private Submenu _pauseMenu;

    [SerializeField]
    private Submenu _loseMenu;

    [SerializeField]
    private Submenu _winMenu;


    public override void Start(){
        base.Start();
        PauseController.Instance.onPaused.AddListener(PauseGame);
        PauseController.Instance.onResumed.AddListener(UnPauseGame);
    }

    public void PauseGame(){
        _pauseMenu.Show();
    }

    public void UnPauseGame(){
        _pauseMenu.Hide();
    }

    public void ShowWinScreen(){
        _winMenu.Show();
    }

    public void ShowLoseScreen(){
        _loseMenu.Show();
    }

}

