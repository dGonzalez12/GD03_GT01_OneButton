﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[RequireComponent(typeof(CanvasGroup))]
public class Submenu : MonoBehaviour{

    CanvasGroup _canvasGroup;

    public UnityEvent OnShow;
    public UnityEvent OnHide;

    private bool isVisible;

    public bool IsVisible => isVisible;


    private void Awake(){
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    public void Hide(){
        OnHide?.Invoke();
        _canvasGroup.blocksRaycasts = false;
        _canvasGroup.interactable = false;
        gameObject.SetActive(false);
        isVisible = false;
        // _canvasGroup.alpha = 0;
    }

    public void Show(){
        // _canvasGroup.alpha = 1;
        gameObject.SetActive(true);
        _canvasGroup.blocksRaycasts = true;
        _canvasGroup.interactable = true;
        OnShow?.Invoke();
        isVisible = true;
    }

}
